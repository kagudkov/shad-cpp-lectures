#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <chrono>

class ExpCounter {
public:
    ExpCounter(std::chrono::system_clock::duration delta);

    void Increment(std::chrono::system_clock::time_point at);
    double GetValue(std::chrono::system_clock::time_point at);

    void Increment();

    double GetValue() {
        return GetValue(std::chrono::system_clock::now());
    }

private:
    double Value_ = 0.0;
};

TEST(ExpCounterTest, Simple) {
    ExpCounter c{std::chrono::seconds(1)};

    auto now = std::chrono::system_clock::now();
    c.Increment(now);

    EXPECT_FLOAT_EQ(1, c.GetValue(now));

    EXPECT_FLOAT_EQ(
        std::exp(-1),
        c.GetValue(now + std::chrono::seconds(2)));
}